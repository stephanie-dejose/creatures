<?php
// ./app/Http/Models/Film.php
// Modèle des films
   namespace App\Http\Models;
   use Illuminate\Database\Eloquent\Model;

   class Film extends Model {
     protected $fillable = ['titre', 'synopsis'];
    /**
     * The table associated with the model.
     * @var string
      */
    protected $table = 'films';
    public function creatures() {
      return $this->hasMany('App\Http\Models\Creature', 'film');
    }
   }
