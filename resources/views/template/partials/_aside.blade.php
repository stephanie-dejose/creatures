{{--
./resources/views/template/partials/_aside.blade.php
--}}

<!-- Sidebar Widgets Column -->
<div class="col-md-4">

  <form action="{{ URL::route('creatures.search') }}" method="GET" role="search">
    {{ csrf_field() }}
    <!-- Search Widget -->
    <div class="card my-4">
      <h5 class="card-header">Rechercher une créature</h5>
      <div class="card-body">
        <div class="input-group">
          <input type="text" class="form-control" placeholder="Mot-clé" name="query" value="{{ request()->input('query') }}">
          <span class="input-group-btn">
            <button class="btn btn-secondary" type="submit">Go!</button>
          </span>
        </div>
      </div>
    </div>
</form>

  <!-- Categories Widget -->
  <div class="card my-4">
    <h5 class="card-header">Tags</h5>
    <div class="card-body">
      <div class="row">
        <div class="col-lg-12">

          <ul class="list-unstyled mb-0">
            @include('tags.index')
          </ul>
        </div>

      </div>
    </div>
  </div>

</div>
