<?php
// ./app/Http/Controllers/FilmsController.php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Http\Models\Film;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;

class FilmsController extends Controller {
  /**
   * Détails d'un film
   * @param  integer $film [film à afficher]
   * @return [view]      [vue films/show.blade.php]
   */
  public function show(Film $film) {
    return View::make('films.show', compact('film'));
  }

    /**
     * Formulaire d'ajout d'un film
     * @return [view]      [vue films/add.blade.php]
     */
  public function add(){
    return View::make('films.add');
  }

  /**
   * Ajout du film
   * @return [view]      [vue pages/show.blade.php]
   */
  public function store(Request $request){
    Film::create($request->all());
  return redirect()->route('pages.show', ['page' => 2, 'slug' => 'les-films']);
  }

  /**
   * Suppression d'un film
   * @param  integer $film [film à supprimer]
   * @return [view]      [vue pages/show.blade.php]
   */
  public function destroy(Film $film){
    $film->destroy($film->id);
    return redirect()->route('pages.show', ['page' => 2, 'slug' => 'les-films']);
  }

  /**
   * Edition d'un film
   * @param  integer $film [film à éditer]
   * @return [view]      [vue films/edit.blade.php]
   */
  public function edit(Film $film){
  return View::make('films.edit', compact('film'));
  }

  /**
   * [update description]
   * @param  Request $request [request]
   * @param  Film    $film    [film à actualiser]
   * @return [view]           [vue pages/show.blade.php]
   */
  public function update(Request $request, Film $film){
  $film->update($request->all());
  return redirect()->route('pages.show', ['page' => 2, 'slug' => 'les-films']);
  }
}
