<?php
// ./app/Http/Controllers/PagesController.php

namespace App\Http\Controllers;
use App\Http\Models\Page as PagesModel;
use App\Http\Models\Creature as CreaturesModel;
use App\Http\Models\Film as FilmsModel;
use Illuminate\Support\Facades\View;

class PagesController extends Controller {
  /**
   * Détails de la page $id
   * @param  integer $id [id de la page à afficher]
   * @return [view]      [vue pages/show.blade.php]
   */
  public function show($id = 1) {
    $page = PagesModel::find($id);
    if($page->id === 1):
      $creatures = CreaturesModel::orderBy('created_at', 'desc')->take(2)->get();
      return View::make('pages.show',compact('page', 'creatures'));
    elseif($page->id === 2):
      $films = FilmsModel::orderBy('created_at', 'desc')->get();
      return View::make('pages.show',compact('page', 'films'));
    elseif($page->id === 3):
      $creatures = CreaturesModel::orderBy('created_at', 'desc')->take(10)->get();
      return View::make('pages.show',compact('page', 'creatures'));
    endif;
    return View::make('pages.show',compact('page'));
  }
}
