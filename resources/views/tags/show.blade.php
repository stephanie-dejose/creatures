{{--
  ./resources/views/tags/show.blade.php
  variables disponibles :
      - $tag Tag
 --}}
@extends('template.defaut')

@section('titre')
  Créatures du {{ $tag->nom }}
@endsection

@section('contenu')
  <!-- Page Content -->
  <div class="container">

    <div class="row">

      <!-- Post Content Column -->
      <div class="col-lg-8">

        <!-- Page Heading -->

        <!-- Title -->
        <h1 class="mt-4">Liste des créatures pour {{ $tag->nom }}</h1>

        <hr>

        @foreach ($tag->creatures as $creature)
          <div class="row">
            <div class="col-md-4">
              <a href="#">
                <img class="img-fluid rounded mb-3 mb-md-0" src="{{ asset('images/'.$creature->image) }}" alt="">
              </a>
            </div>
            <div class="col-md-8">
              <h3>{{ $creature->nom }}</h3>
              <p class="lead">
                dans
                <a href="{{ URL::route('films.show', ['film' => $creature->filmData->id, 'slug' => Str::slug($creature->filmData->titre, '-')]) }}">{{ $creature->filmData->titre }}</a> le {{ \Carbon\Carbon::parse($creature->created_at)->format('D d M y') }}
              </p>
              <p>{{ Str::words($creature->texteLead, 20, ' ...') }}</p>
              <a class="btn btn-primary" href="{{ URL::route('creatures.show', ['creature' => $creature->id, 'slug' => Str::slug($creature->nom, '-')]) }}">Voir la créature</a>
              <hr/>
              <ul class="list-inline tags">
                @foreach ($creature->tags as $tag)
                  <li>
                    <a href="{{ route('tags.show', [
                                      'tag'  => $tag->id,
                                      'slug' => Str::slug($tag->nom, '-')
                                      ])}}"class="btn btn-default btn-xs">
                             {{ $tag->nom }}</a>
                  </li>
                @endforeach
              </ul>


      </div>
      </div>
      <hr>
      @endforeach
    </div>

@endsection
