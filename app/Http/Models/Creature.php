<?php
// ./app/Http/Models/Creature.php
// Modèle des créatures
   namespace App\Http\Models;
   use Illuminate\Database\Eloquent\Model;

   class Creature extends Model {
     /**
      * The table associated with the model.
      * @var string
      */
     protected $table = 'creatures';
     public function filmData() {
       return $this->belongsTo('App\Http\Models\Film', 'film');
     }
     public function tags() {
       return $this->belongsToMany('App\Http\Models\Tag', 'creatures_has_tags', 'creature', 'tag');
     }
   }
