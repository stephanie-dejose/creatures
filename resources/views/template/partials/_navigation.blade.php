{{--
./resources/views/template/partials/_navigation.blade.php
--}}

<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
  <div class="container">
    <a class="navbar-brand" href="{{ URL::route('accueil') }}">Vintage Sci-Fi - Les Créatures du Futur</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      @include('pages.menu')
    </div>
  </div>
</nav>
