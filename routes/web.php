<?php
/*
  ./routes/web.php
  Routeur
*/


/*-----------------------------------
    CREATURES
-----------------------------------*/

Route::prefix('creatures')->name('creatures.')->group(function(){
  /*
    DETAILS D'UNE CREATURE
    PATERN: /creatures/{creature}/{slug}.html
    CTRL: CreaturesController
    ACTION: show
   */
  Route::get('{creature}/{slug}.html', 'CreaturesController@show')
         ->where([
                  'creature'   => '[1-9][0-9]*',
                  'slug' => '[a-z0-9][a-z0-9\-]*'
                ])
         ->name('show');

  /*
    RECHERCHE D'UNE CREATURE
    PATERN: /creatures/search
    CTRL: CreaturesController
    ACTION: search
   */
  Route::get('search', 'CreaturesController@search')->name('search');

});


/*-----------------------------------
    FILMS
-----------------------------------*/
Route::prefix('films')->name('films.')->group(function(){
/*
  DETAILS D'UN FILM
  PATTERN: /films/{film}/{slug}.html [GET]
  CTRL: Films
  ACTION: show
 */
Route::get('{film}/{slug}.html', 'FilmsController@show')
      ->where([
               'film'   => '[1-9][0-9]*',
               'slug' => '[a-z0-9][a-z0-9\-]*'
             ])
      ->name('show');

/*
  FORMULAIRE D'AJOUT D'UN FILM
  PATTERN: /films/add [GET]
  CTRL: Films
  ACTION: add
 */
  Route::get('add','FilmsController@add')->name('add');

/*
  AJOUT D'UN FILM
  PATTERN: /films [POST]
  CTRL: Films
  ACTION: store
 */
Route::post('', 'FilmsController@store')->name('store');

/*
  EDITION D'UN FILM
  PATTERN: /films/{film}/{slug}/edit.html    [POST]
  CTRL: Films
  ACTION: edit
 */
Route::get('{film}/{slug}/edit.html', 'FilmsController@edit')
        ->where([
          'film' => '[1-9][0-9]*',
          'slug' => '[a-z0-9][a-z0-9\-]*'
        ])
        ->name('edit');

/*
  UPDATE D'UN FILM
  PATTERN: /films/id/slug.html [PATCH]
  CTRL: Films
  ACTION: update
 */
 Route::patch('/films/{film}/{slug}.html','FilmsController@update')
          ->where([
             'film' => '[1-9][0-9]*',
             'slug' => '[a-z0-9][a-z0-9\-]*'
            ])
         ->name('update');
/*
SUPPRESSION D'UN FILM
 PATTERN: /films/{film}/{slug} [DELETE]
 CTRL: Films
 ACTION: destroy
*/
Route::delete('/films/{film}/{slug}.html', 'FilmsController@destroy')
      ->where([
         'film' => '[1-9][0-9]*',
         'slug' => '[a-z0-9][a-z0-9\-]*'
        ])
      ->name('destroy');
});

/*-----------------------------------
    TAGS
-----------------------------------*/
/*
  DETAILS D'UN TAG
  PATTERN: /tags/{tag}/{slug}.html [GET]
  CTRL: Tags
  ACTION: show
 */
Route::get('/tags/{tag}/{slug}.html', 'TagsController@show')
      ->where([
               'tag'   => '[1-9][0-9]*',
               'slug' => '[a-z0-9][a-z0-9\-]*'
             ])
      ->name('tags.show');

/*-----------------------------------
    PAGES
-----------------------------------*/
/*
 DETAILS D'UNE PAGE
 PATERN: /pages/{page}/{slug}.html
 CTRL: PagesController
 ACTION: show
*/
Route::get('/pages/{page}/{slug}.html', 'PagesController@show')
     ->where([
               'page'   => '[1-9][0-9]*',
               'slug' => '[a-z0-9][a-z0-9\-]*'
             ])
      ->name('pages.show');

//==============================================================

/*
  MISE A DISPOSITION DE DONNEES POUR CERTAINES VUES
*/
View::composer('pages.menu', function($view){
  $view->with('pages', App\Http\Models\Page::all());
});

View::composer('tags.index', function($view){
  $view->with('tags', App\Http\Models\Tag::all());
});

/*
  ROUTE PAR DEFAUT
  PATERN: /
  CTRL: PagesController
  ACTION: show
 */
Route::get('/', 'PagesController@show')->name('accueil');
