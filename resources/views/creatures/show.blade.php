{{--
./resources/views/creatures/show.blade.php
Description: Détails d'une créature
Données disponibles :
 - $creature : OBJ(id, nom, texteLead, texteSuite, created_at, image, film, updated_at)
  -
--}}
@extends('template.defaut')

@section('titre')
  {{ $creature->nom }}
@stop

@section('contenu')
  <!-- Page Content -->
  <div class="container">

    <div class="row">

      <!-- Post Content Column -->
      <div class="col-lg-8">

        <!-- Page Heading -->

        <!-- Title -->
        <h1 class="mt-4">{{ $creature->nom }}</h1>
        <p class="lead">
          dans
          <a href="{{ URL::route('films.show', ['film' => $creature->filmData->id, 'slug' => Str::slug($creature->filmData->titre, '-')]) }}">{{ $creature->filmData->titre }}</a> le {{ \Carbon\Carbon::parse($creature->created_at)->format('D d M y') }}
        </p>

        <hr>

        <!-- Project -->
        <div class="row">
          <div class="col-md-6">
            <a href="#">
              <img class="img-fluid rounded mb-3 mb-md-0" src="{{ asset('images/'.$creature->image) }}" alt="">
            </a>
          </div>
          <div class="col-md-6">
            <p class="lead">{{ $creature->texteLead }}</p>
            <hr/>
            <p>{{ $creature->texteSuite }}</p>

            <hr/>
            <ul class="list-inline tags">
              @foreach ($creature->tags as $tag)
                <li>
                  <a href="{{ URL::route('tags.show', [
                                    'tag'  => $tag->id,
                                    'slug' => Str::slug($tag->nom, '-')
                                    ])}}"class="btn btn-default btn-xs">
                           {{ $tag->nom }}</a>
                </li>
              @endforeach
            </ul>
          </div>
        </div>
        <!-- /.row -->
        <hr>
      </div>

@stop
