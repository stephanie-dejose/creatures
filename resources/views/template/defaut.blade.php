{{--
./resources/views/template/defaut.blade.php
Template général
Variables disponibles
-
--}}
<!DOCTYPE html>
<html lang="en">

<!-- Head -->
@include('template.partials._head')

  <body>

    <!-- Navigation -->
    @include('template.partials._navigation')

    <!-- Page Content -->
    <div class="container">

      @yield('contenu')

      <!-- Aside -->
      @include('template.partials._aside')
    </div>
    <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- Footer -->
    @include('template.partials._footer')

    <!-- Scripts -->
    @include('template.partials._scripts')

  </body>

</html>
