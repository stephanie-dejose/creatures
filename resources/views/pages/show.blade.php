{{--
./resources/views/pages/show.blade.php
Variables disponibles :
  $page: OBJ(id, titre, texte, tri, created_at, updated_at)
  - Si $page->id = 1 : $creatures : Array(OBJ(id, nom, texteLead, texteSuite, created_at, image, film, updated_at))
--}}
@extends('template.defaut')
@section('titre')
 {{ $page->titre }}
@stop

@section('contenu')
  <div class="row">

    <!-- Post Content Column -->
    <div class="col-lg-8">

      <!-- Page Heading -->

      <!-- Title -->
      <h1 class="mt-4">{{ $page->titre }}</h1>

      <hr>
      <!-- Post Content -->
      <p class="lead">{!! html_entity_decode($page->texte) !!}</p>

      <hr>
      <!-- Intégration des vues complémentaires -->

  @if($page->id === 1)
    @include('creatures.index')
  @elseif ($page->id === 2)
    @include('films.index')
  @elseif($page->id === 3)
    @include('creatures.index')
  @endif


    </div>


@stop
