<?php
// ./app/Http/Models/Tag.php
// Modèle des tags
namespace App\Http\Models;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model {
  /**
   * The table associated with the model.
   * @var string
   */
  protected $table = 'tags';
  protected $fillable = ['nom'];
  public function creatures() {
   return $this->belongsToMany('App\Http\Models\Creature', 'creatures_has_tags', 'tag', 'creature');
  }
}
