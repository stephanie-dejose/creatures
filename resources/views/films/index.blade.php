{{--
./resources/views/films/index.blade.php
Description: Liste des films
Variables disponibles :
  - $films : Array(OBJ(id, titre, synopsis, created_at, updated_at, image))
--}}
<a class="btn btn-primary" href="{{ URL::route('films.add') }}">Ajouter un film</a>
<hr/>
@foreach ($films as $film)
  <div class="row">
    <div class="col-md-4">
      <a href="#">
        <img class="img-fluid rounded mb-3 mb-md-0" src="{{ asset('images/'.$film->image) }}" alt="">
      </a>
    </div>
    <div class="col-md-8">
      <h3>{{ $film->titre }}</h3>
      <p>{{ Str::words($film->synopsis, 50, ' ...') }}</p>
      <a class="btn btn-primary" href="{{ URL::route('films.show', ['film' => $film->id, 'slug' => Str::slug($film->titre, '-')]) }}">Voir les détails du film</a>
    </div>
  </div>
  <hr/>
@endforeach
