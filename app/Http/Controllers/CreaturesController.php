<?php
// ./app/Http/Controllers/CreaturesController.php

namespace App\Http\Controllers;
use App\Http\Models\Creature;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;

class CreaturesController extends Controller {
  /**
   * Détails d'une créature
   * @param  Creature $creature [créature à afficher]
   * @return [view]             [vue creatures/show.blade.php]
   */
  public function show(Creature $creature) {
    return View::make('creatures.show', compact('creature'));
  }
  /**
   * Recherche d'une créature
   * @param  Request $request [requête]
   * @return [view]           [vue creatures/search.blade.php]
   */
  public function search(Request $request) {
    $query = $request->input('query');
    $creatures = Creature::where('nom', 'like', "%$query%")->get();

    return view('creatures.search')->with('creatures', $creatures);
  }
}
