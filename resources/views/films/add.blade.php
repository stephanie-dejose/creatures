{{--
./resources/views/films/add.blade.php
Description: Ajout d'un film
Variables disponibles :
  - $films : Array(OBJ(id, titre, synopsis, created_at, updated_at, image))
--}}
@extends('template.defaut')

@section('titre')
  Ajouter un film
@endsection

@section('contenu')

<div class="container">

  <div class="row">

    <!-- Post Content Column -->
    <div class="col-lg-8">
      <h1 class="mt-4">Ajouter un Film</h1>

      <form method="POST" action="{{ route('films.store') }}">
        @csrf
        <div class="form-group">
          <label for="">Titre</label>
          <input type="text" class="form-control" name="titre"/>
        </div>
        <div class="form-group">
          <label for="">Synopsis</label>
          <textarea rows="8" cols="80" class="form-control" name="synopsis"></textarea>
        </div>
        <div class="form-group">
          <input type="submit" class="btn btn-primary" />
        </div>
      </form>

      <!-- /.row -->
      <hr>
    </div>
@endsection
