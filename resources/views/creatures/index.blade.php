{{--
./resources/views/creatures/index.blade.php
Description: Liste des créatures
Variables disponibles :
  - $creatures : Array(OBJ(id, nom, texteLead, texteSuite, created_at, image, film, updated_at))
--}}
  @if($page->id === 1)
<!-- Title -->
<h2 class="mt-4">Dernieres créatures</h2>
<hr/>
@endif

@foreach ($creatures as $creature)
  <div class="row">
    <div class="col-md-4">
      <a href="#">
        <img class="img-fluid rounded mb-3 mb-md-0" src="{{ asset('images/'.$creature->image) }}" alt="">
      </a>
    </div>
    <div class="col-md-8">
      <h3>{{ $creature->nom }}</h3>
      <p class="lead">
        dans
        <a href="{{ URL::route('films.show', ['film' => $creature->filmData->id, 'slug' => Str::slug($creature->filmData->titre, '-')]) }}">{{ $creature->filmData->titre }}</a> le {{ \Carbon\Carbon::parse($creature->created_at)->format('D d M y') }}
      </p>
      <p>{{ Str::words($creature->texteLead, 20, ' ...') }}</p>
      <a class="btn btn-primary" href="{{ URL::route('creatures.show', ['creature' => $creature->id, 'slug' => Str::slug($creature->nom, '-')]) }}">Voir la créature</a>
      <hr/>
      <ul class="list-inline tags">
        @foreach ($creature->tags as $tag)
          <li>
            <a href="{{ route('tags.show', [
                              'tag'  => $tag->id,
                              'slug' => Str::slug($tag->nom, '-')
                              ])}}"class="btn btn-default btn-xs">
                     {{ $tag->nom }}</a>
          </li>
        @endforeach
      </ul>
    </div>
  </div>
  <!-- /.row -->

  <hr>
@endforeach
