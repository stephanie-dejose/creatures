<?php
// ./app/Http/Controllers/CreaturesController.php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use App\Http\Models\Tag;

class TagsController extends Controller {
  /**
   * Liste des tags
   * @return [view]      [vue tags/index.blade.php]
   */
  public function index() {
    $tags = Tag::all();
    return View::make('tags.index', compact('tags'));
  }

  /**
   * Détails d'un tag
   * @param  integer $tag [tag de la page à afficher]
   * @return [view]      [vue tags/show.blade.php]
   */
  public function show(Tag $tag) {
    return View::make('tags.show', compact('tag'));
  }
}
