{{--
./resources/views/pages/menu.blade.php
Données disponibles :
- $tags: OBJ(id, nom, created_at, updated_at)
--}}
  @foreach ($tags as $tag)
    <li>
      <a href="{{ URL::route('tags.show', [
          'tag'  => $tag->id,
          'slug' => Str::slug($tag->nom)
        ]) }}">
        {{ $tag->nom }}
      </a>
    </li>
  @endforeach
