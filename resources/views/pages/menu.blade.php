{{--
./resources/views/pages/menu.blade.php
Données disponibles :
- $pages: ARRAY(OBJ(id, titre, texte, tri, created_at, updated_at))
--}}
<ul class="navbar-nav ml-auto">
  @foreach ($pages as $page)
    <li class="nav-item {{ (request()->segment(2) == $page->id) ? 'active' : '' }}">
      <a class="nav-link" href="{{ URL::route('pages.show', ['page' => $page->id, 'slug' => Str::slug($page->titre, '-')]) }}">
        {{ $page->titre }}
      </a>
    </li>
  @endforeach
</ul>
